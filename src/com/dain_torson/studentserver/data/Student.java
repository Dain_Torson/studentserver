package com.dain_torson.studentserver.data;

import java.util.Arrays;

public class Student {

    public static int numOfSems = 10;
    private String firstName;
    private String secondName;
    private String lastName;
    private String group;
    private String socWorks [] = new String[numOfSems];

    public Student() {
        initialize();
    }

    public Student(String firstName, String secondName, String lastName, String group) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.group = group;
        initialize();
    }

    public Student(String firstName, String secondName, String lastName, String group, String [] workList) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.group = group;
        initialize();

        if(workList.length == numOfSems) {
            this.socWorks = workList;
        }
        else {
            for(int workIdx = 0; workIdx < workList.length; ++workIdx) {
                if(workIdx >= numOfSems) {
                    break;
                }
                socWorks[workIdx] = workList[workIdx];
            }
        }
    }

    private void initialize() {
        for (int workIdx = 0; workIdx < numOfSems; ++workIdx) {
            socWorks[workIdx] = "";
        }
    }

    public boolean setWork(String work, int sem) {
        if(sem < numOfSems && sem >= 0) {
            socWorks[sem] = work;
            return true;
        }
        return false;
    }

    public String getWork(int sem) {
        if(sem < numOfSems && sem >= 0) {
            return socWorks[sem];
        }
        return "";
    }

    public String[] getSocWorks() {
        return socWorks;
    }

    public void setSocWorks(String[] socWorks) {
        this.socWorks = socWorks;
    }

    public String getFmlName() {
        return lastName + " " + firstName + " " + secondName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (firstName != null ? !firstName.equals(student.firstName) : student.firstName != null) return false;
        if (group != null ? !group.equals(student.group) : student.group != null) return false;
        if (lastName != null ? !lastName.equals(student.lastName) : student.lastName != null) return false;
        if (secondName != null ? !secondName.equals(student.secondName) : student.secondName != null) return false;
        if (!Arrays.equals(socWorks, student.socWorks)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (socWorks != null ? Arrays.hashCode(socWorks) : 0);
        return result;
    }
}

