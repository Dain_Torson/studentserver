package com.dain_torson.studentserver;

import com.dain_torson.studentserver.controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StudentServer extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("view/view.fxml").openStream());
        primaryStage.setTitle("Student Server");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 400, 300));
        Controller controller = fxmlLoader.getController();
        controller.setScene(primaryStage.getScene());
        primaryStage.show();
        controller.run();
    }


    public static void main(String[] args) {
        launch(args);
    }
}