package com.dain_torson.studentserver.controller.exceptions;

public class DataTransferFailedException extends Exception {

    private String message = "Data transfer failed";

    @Override
    public String getMessage() {
        return message;
    }
}
