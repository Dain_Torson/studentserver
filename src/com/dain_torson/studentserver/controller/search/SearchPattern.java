package com.dain_torson.studentserver.controller.search;

import com.dain_torson.studentserver.data.Student;

public class SearchPattern {

    public Student source;
    public int minWorks;
    public int maxWorks;
    public boolean delete;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchPattern that = (SearchPattern) o;

        if (delete != that.delete) return false;
        if (maxWorks != that.maxWorks) return false;
        if (minWorks != that.minWorks) return false;
        if (source != null ? !source.equals(that.source) : that.source != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = source != null ? source.hashCode() : 0;
        result = 31 * result + minWorks;
        result = 31 * result + maxWorks;
        result = 31 * result + (delete ? 1 : 0);
        return result;
    }
}
