package com.dain_torson.studentserver.controller;

import com.dain_torson.studentserver.controller.datatransfer.DataTransferController;
import com.dain_torson.studentserver.controller.file.XMLFileProcessor;
import com.dain_torson.studentserver.data.Student;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.WindowEvent;

import java.io.File;
import java.util.List;

public class Controller {

    private String dataPath = "d:\\NewTable.xml";
    private List<Student> students;
    private DataTransferController transferController;
    private static Scene scene = null;

    @FXML
    private MenuItem restartServerItem;
    @FXML
    private MenuItem stopServerItem;
    @FXML
    private MenuItem exitItem;
    @FXML
    private TextArea logArea;
    @FXML
    private Button updateLogButton;

    @FXML
    private void initialize() {

        File file = new File(dataPath);
        XMLFileProcessor processor = new XMLFileProcessor(file);
        students = processor.open();
        updateLogButton.setOnAction(new UpdateButtonHandler());
        restartServerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                transferController.restart();
                restartServerItem.setDisable(true);
                stopServerItem.setDisable(false);
            }
        });
        stopServerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stop();
            }
        });
        exitItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stop();
                Platform.exit();
            }
        });

        restartServerItem.setDisable(true);
    }

    public void run() {
        transferController = new DataTransferController(students, dataPath);
        transferController.start();
    }

    private void stop() {
        transferController.cancel();
        restartServerItem.setDisable(false);
        stopServerItem.setDisable(true);
    }

    public static Scene getScene() {
        return scene;
    }

    public void setScene(Scene new_scene) {
        scene = new_scene;
        scene.getWindow().setOnCloseRequest(new WindowClosedEventHandler());
    }

    private class UpdateButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            logArea.clear();
            for(String message : transferController.getLog()) {
                logArea.appendText(message + "\n");
            }
        }
    }

    private class WindowClosedEventHandler implements EventHandler<WindowEvent> {

        @Override
        public void handle(WindowEvent event) {
            stop();
        }
    }
}
