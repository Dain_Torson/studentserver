package com.dain_torson.studentserver.controller.datatransfer;


import com.dain_torson.studentserver.controller.exceptions.DataTransferFailedException;
import com.dain_torson.studentserver.controller.file.XMLFileProcessor;
import com.dain_torson.studentserver.data.Student;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataTransferController extends Service<Integer> {

    private int port = 6600;
    private ServerSocket serverSocket = null;
    private Socket socket = null;
    private Sender sender;
    private List<Student> students;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private String dataPath;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private List<String> log = new ArrayList<String>();


    public DataTransferController(List<Student> students, String dataPath) {
        this.students = students;
        this.dataPath = dataPath;
    }

    private void establishConnection() {
        try {
            serverSocket = new ServerSocket(port);
            socket = serverSocket.accept();
            updateLog("Connection established with: " + socket.getInetAddress().toString() + "::"
                    + String.valueOf(socket.getPort()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            sender = new Sender(in, out, students);
        } catch (IOException exception) {
            closeConnection();
        }
    }

    private void closeConnection() {
        try {
            if(sender != null) {
                sender.close();
            }
            if(socket != null) {
            updateLog("Connection closed with: " + socket.getInetAddress().toString() + "::"
                            + String.valueOf(socket.getPort()));

                socket.close();
            }
            serverSocket.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void save() {

        File file = new File(dataPath);
        XMLFileProcessor processor = new XMLFileProcessor(file);
        processor.save(students);
    }

    @Override
    protected Task<Integer> createTask() {
        return new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                establishConnection();
                String input;
                while (!isCancelled()) {
                    try {
                        if ((input = in.readLine()) != null) {
                            if (isCancelled()) {
                                break;
                            }
                            if (input.equalsIgnoreCase("exit")) {
                                closeConnection();
                                establishConnection();
                            } else if (input.equalsIgnoreCase("get_data")) {
                                try {
                                    updateLog("Data has been requested");
                                    sender.sendData();
                                    updateLog("Data transfer successfully finished");
                                }
                                catch (DataTransferFailedException exception) {
                                    updateLog(exception.getMessage());
                                }
                            } else if (input.equalsIgnoreCase("get_data_size")) {
                                try {
                                    updateLog("Data size has been requested");
                                    sender.sendDataSize();
                                    updateLog("Data transfer successfully finished");
                                }
                                catch (DataTransferFailedException exception) {
                                    updateLog(exception.getMessage());
                                }
                            } else if (input.equalsIgnoreCase("add_record")) {
                                try {
                                    updateLog("New data addition has been requested");
                                    sender.addRecord();
                                    updateLog("Data transfer successfully finished");
                                }
                                catch (DataTransferFailedException exception) {
                                    updateLog(exception.getMessage());
                                }
                            } else if (input.equalsIgnoreCase("save")) {
                                updateLog("Data saving requested");
                                save();
                                updateLog("Data saving successfully finished");
                            }
                        }
                    } catch (IOException exception) {
                        closeConnection();
                        return -1;
                    }
                }
                closeConnection();
                return 0;
            }
        };
    }

    public List<String> getLog() {
        return log;
    }

    private void updateLog(String message) {

        if(log.size() > 50) {
            log.clear();
        }
        Date date = new Date();
        log.add(dateFormat.format(date) + " | " + message);
    }
}

