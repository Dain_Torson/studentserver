package com.dain_torson.studentserver.controller.datatransfer;

import com.dain_torson.studentserver.controller.exceptions.DataTransferFailedException;
import com.dain_torson.studentserver.controller.search.RecordSearcher;
import com.dain_torson.studentserver.controller.search.SearchPattern;
import com.dain_torson.studentserver.data.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Sender {

    private BufferedReader in;
    private PrintWriter out;
    private List<Student> students;
    private List<Student> dataToSend;
    private SearchPattern previousPattern = null;

    public Sender(BufferedReader input, PrintWriter output, List<Student> students) {

        this.in = input;
        this.out = output;
        this.students = students;
        this.dataToSend = this.students;
    }

    private SearchPattern createSearchPattern(){

        SearchPattern searchPattern = new SearchPattern();
        Student student;

        try {
            String name = in.readLine();
            if(!name.equalsIgnoreCase("no_pattern")) {
                student = new Student();
                if(name == null) {
                    student.setLastName("");
                }
                else {
                    student.setLastName(name);
                }

                String group = in.readLine();
                if(group == null) {
                    student.setGroup("");
                }
                else {
                    student.setGroup(group);
                }

                String work = in.readLine();
                if(work != null) {
                    student.setWork(work, 0);
                }
                searchPattern.source = student;
                searchPattern.minWorks = Integer.valueOf(in.readLine());
                searchPattern.maxWorks = Integer.valueOf(in.readLine());
                searchPattern.delete = Boolean.valueOf(in.readLine());
                return searchPattern;
            }
            else {
                searchPattern.delete = false;
            }
        }
        catch (IOException exception) {
            return null;
        }

       return null;
    }

    private void deleteRecords(List<Student> records) {

        for(Student record : records) {
            students.remove(record);
        }
    }

    public void sendData() throws DataTransferFailedException {

        SearchPattern pattern = createSearchPattern();
        if(pattern == null) {
            dataToSend = students;
        }
        else if(!pattern.equals(previousPattern)) {
            RecordSearcher searcher = new RecordSearcher(pattern.source, students,
                    pattern.minWorks, pattern.maxWorks);
            dataToSend = searcher.search();
            previousPattern = pattern;
        }
        else {
            System.out.println("Equals");
        }

        int pageNumber;
        int recPerPage;
        try {
            pageNumber = Integer.valueOf(in.readLine());
            recPerPage = Integer.valueOf(in.readLine());
        }
        catch (IOException exception) {
            throw new DataTransferFailedException();
        }
        catch (NumberFormatException exception) {
            throw new DataTransferFailedException();
        }

        int start = recPerPage * pageNumber;
        for(int recordIdx = start; recordIdx < start + recPerPage; ++recordIdx) {
            if(recordIdx >= dataToSend.size()) {
                break;
            }

            out.println(dataToSend.get(recordIdx).getFirstName());
            out.println(dataToSend.get(recordIdx).getSecondName());
            out.println(dataToSend.get(recordIdx).getLastName());
            out.println(dataToSend.get(recordIdx).getGroup());

            String works [] = dataToSend.get(recordIdx).getSocWorks();
            for(int workIdx = 0; workIdx < Student.numOfSems; ++workIdx ) {
                out.println(works[workIdx]);
            }
        }

        if(pattern != null && pattern.delete) {
            deleteRecords(dataToSend);
        }
        out.println("transmission_finished");

        if(out.checkError()) {
            throw new DataTransferFailedException();
        }
    }

    public void addRecord() throws DataTransferFailedException{

        try {
            String name = in.readLine();
            Student student = new Student();
            student.setFirstName(name);
            student.setSecondName(in.readLine());
            student.setLastName(in.readLine());
            student.setGroup(in.readLine());

            String works [] = new String[Student.numOfSems];
            for(int workIdx = 0; workIdx < Student.numOfSems; ++workIdx) {
                works[workIdx] = in.readLine();
            }

            student.setSocWorks(works);
            students.add(student);
        }
        catch (IOException exception) {
            throw new DataTransferFailedException();
        }
    }

    public void sendDataSize() throws DataTransferFailedException{

        out.println(String.valueOf(dataToSend.size()));
        if(out.checkError()) {
            throw new DataTransferFailedException();
        }
    }

    public void save() {

    }

    public void close() {

        try {
            in.close();
            out.close();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
